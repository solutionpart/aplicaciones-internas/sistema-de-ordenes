package com.solutionpart.internalsystem.controller;


import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.solutionpart.internalsystem.model.HouseholdAppliance;
import com.solutionpart.internalsystem.model.servicesOrder;

@Controller
public class OrderController {
	
	 	
	@GetMapping("/orderform")
    public String clientData(Model model) {
		ArrayList<String> typeList = new ArrayList<String>();
		ArrayList<String> brandList = new ArrayList<String>();
		typeList.add("Lavadora");
		brandList.add("Electrolux");
		servicesOrder servicesOrder = new servicesOrder();
        model.addAttribute("servicesOrder", servicesOrder);
        model.addAttribute("typeList",typeList);
        model.addAttribute("brandList", brandList);
        return "orderform";
    }

    @PostMapping("/orderform")
    public String orderSubmit(@ModelAttribute servicesOrder order){
    	
        return "result";
    }
    
 
}
