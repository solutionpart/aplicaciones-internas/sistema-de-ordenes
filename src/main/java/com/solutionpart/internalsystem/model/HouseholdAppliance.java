package com.solutionpart.internalsystem.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


import lombok.Data;

@Data
@Entity
@Table(name="householdappliance")
public class HouseholdAppliance {
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy="uuid2")
	private String applianceId;
	private String type;
	private String model;
	private String brand;
	@ManyToOne
	@JoinColumn(name="orderId")
	private servicesOrder order;
	
	public HouseholdAppliance() {
		this.order= new servicesOrder();
	}
	
}
