package com.solutionpart.internalsystem.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@SuppressWarnings("unused")
@Entity
@Table(name= "client")
@NamedQuery(name="Client.findByDNI",query="Select c from Client c where c.clientId = ?1")
public class Client {
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy="uuid2")
	private String clientId;
	private String clientName;
	private String clientLastName;
	private String clientDNI;
	private String clientAddress;
	private String clientCellPhone;
	private String clientPhone;
	private String clientEmail;
	
	@OneToMany(mappedBy = "client")
	private Set<servicesOrder> orders;
	
	public Client() {
		
	}
}
