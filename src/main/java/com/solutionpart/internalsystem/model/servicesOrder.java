package com.solutionpart.internalsystem.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;


import lombok.Data;

@Data
@Entity
@Table(name="servicesorder")
public class servicesOrder {
	@Id
	@GeneratedValue(generator ="system-uuid")
	@GenericGenerator(name="system-uuid", strategy="uuid2")
	private String orderId;
	@Temporal(TemporalType.DATE)
	private Date openDate;
	@Temporal(TemporalType.DATE)
	private Date closeDate;
	private String status;
	private String orderReport;
	@ManyToOne
	@JoinColumn(name="clientId")
	private Client client;
	
	@OneToMany(mappedBy = "order")
	private Set<HouseholdAppliance> householdAppliances;
	
	
}
