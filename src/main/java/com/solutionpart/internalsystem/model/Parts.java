package com.solutionpart.internalsystem.model;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class Parts {
	private String id;
	private String partType;
	private String partName;
	private String partNumber;
}
