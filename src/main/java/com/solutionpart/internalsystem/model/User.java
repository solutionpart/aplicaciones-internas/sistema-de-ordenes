package com.solutionpart.internalsystem.model;

import lombok.Data;

@Data
@SuppressWarnings("unused")
public class User {
	private String userId;
	private String userName;
	private String userPassword;
	private String userPrivileges;
}
