package com.solutionpart.internalsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolutionpartApplication {

	public static void main(String[] args) {
		SpringApplication.run(SolutionpartApplication.class, args);
	}

}
